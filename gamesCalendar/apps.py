from django.apps import AppConfig


class GamescalendarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gamesCalendar'
