from django.shortcuts import render
from django.views.generic import View


class GamesCalendar(View):
    def get(self, request):
        return render(request, 'calendar.html', {
            'extraContext': {
                'title': 'Calendar'
            },
        })
