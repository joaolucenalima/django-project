from django.urls import path

from gamesCalendar.views import GamesCalendar

urlpatterns = [
  path("", GamesCalendar.as_view(), name="gamesCalendar")
]