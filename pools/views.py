from django.shortcuts import render, redirect
from django.views.generic import View
from django.urls import reverse
from django.utils import timezone

from pools.filters import PoolFilter
from pools.forms import PoolForm

import uuid

# Create your views here.
class PoolsListView(View):
    def get(self, request):
        filter_class = PoolFilter(request.GET, request=request)

        return render(request, 'pools.html', {
            'extraContext': {
                'title': 'Pools'
            },
            "context": {
                "filter": filter_class 
            }
        })



class PoolCreateView(View):
  def post(self, request):
    create_form = PoolForm(request.POST)

    if (create_form.is_valid()):
      create_form.save()

      return redirect(
        f"{reverse('pools-list')}"
      )
    
    return render(request, "pools/pools.html", {
      "extraContext": {
        "title": "Pools"
      },
      "context": {
        "filter": create_form 
      }
    })

  def get(self, request):
    create_form = PoolForm(
      initial={
        "id": str(uuid.uuid4()),
        "created_at": timezone.now()
      }
    )

    return render(request, "pools/pools.html", {
      "extraContext": {
        "title": "Pools"
      },
      "context": {
        "filter": create_form 
      }
    })
  