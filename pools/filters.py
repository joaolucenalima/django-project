import django_filters
from django import forms

from pools.models import Pool


class PoolFilter(django_filters.FilterSet):
  code = django_filters.CharFilter(
      lookup_expr='exact',
      widget=forms.TextInput(
          attrs={
                'placeholder': 'AAAA-BBBB-CCCC-DDDD'
            }
      )
  )

  class Meta:
    model = Pool
    fields = "__all__"