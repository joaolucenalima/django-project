from django import forms

from pools.models import Pool


class PoolForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-field',
                'placeholder': 'Ex.: Bolão da família',
            }
        )
    )

    code = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'AAAA-BBBB-CCCC-DDDD'
            }
        )
    )

    class Meta:
        model = Pool
        fields = '__all__'