from django.urls import path

from pools.views import PoolsListView

urlpatterns = [
  path("", PoolsListView.as_view(), name="pools-list")
]