$(document).ready(() => {
  const createPoolButton = $('#create-pool');
  const modal = $('dialog')[0];
  const closeModalButton = $('.close-button');

  createPoolButton.on('click', () => {
    modal.showModal();
  })

  closeModalButton.on('click', () => {
    modal.close();
  })
})